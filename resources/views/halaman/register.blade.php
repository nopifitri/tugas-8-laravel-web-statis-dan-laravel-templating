<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <p>First Name :</p>
        <input type="text" name="nama_depan">
        <p>Last Name : </p>
        <input type="text" name="nama_belakang">
        <p>Gender</p>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female
        <p>Nationality</p>
        <select name="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="US">America</option>
            <option value="Jepang">Japan</option>
            <option value="Korea">Korea</option>
        </select>
        <p>Language Spoken</p>
        <input type="checkbox" name="indo">Bahasa Indonesia<br>
        <input type="checkbox" name="inggris">English<br>
        <input type="checkbox" name="other">Other<br>
        <p>Bio</p>
        <textarea name="" id="" cols="30" rows="10"></textarea>
        <br>
        <input type="submit" value="Submit">
    </form>
</body>
</html>