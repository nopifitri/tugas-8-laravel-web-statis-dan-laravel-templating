@extends('layout.master')
@section('judul')
Halaman Edit Pemeran
@endsection

@section('content')
    <form method="post" action="/cast/{{$cast->id}}">
        @csrf
        @method('put')
        {{-- nama --}}
        <div class="form-group">
            <label >Nama Pemeran</label>
            <input type="text" name="nama" value="{{$cast->nama}}" class="form-control" >
        </div>
        {{-- untuk memunculkan alert jika validasi salah --}}
        @error('nama') 
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        {{-- umur --}}
        <div class="form-group">
            <label >Umur Pemeran</label>
            <input type="number" name="umur" value="{{$cast->umur}}" class="form-control" >
        </div>
        {{-- untuk memunculkan alert jika validasi salah --}}
        @error('umur') 
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        {{-- bio --}}
        <div class="form-group">
            <label>Biodata</label>
            <textarea name="bio" cols="30" rows="10" class="form-control">{{$cast->bio}}</textarea>        
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </form>
@endsection