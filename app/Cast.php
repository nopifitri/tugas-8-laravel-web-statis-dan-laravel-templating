<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table = "cast"; //berfungsi mengarahkan model ke table post di database
    protected $fillable = ["nama", "umur", "bio"]; //berfungsi Kolom apa saja yang akan di manipulasi
}
