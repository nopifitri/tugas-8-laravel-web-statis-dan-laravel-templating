<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Kak ini tugas release 1 nya. Sengaja saya comment
karena binggung jika diubah route nya. Karena pada soal diminta route untuk
home yaitu '/' nah takut ketabrak dengan tugas release 2 nya yang juga '/' dengan template

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@form');
Route::post('/welcome', 'AuthController@signUp');
*/

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@form');
Route::post('/welcome', 'AuthController@signUp');
Route::get('/data-table', 'IndexController@viewTable');

//Route CRUD
//1. create
Route::get('/cast/create', 'CastController@create'); //mengarah ke form untuk isi data ke database
Route::post('cast', 'CastController@store'); //menyimpan data baru ke tabel cast

//2. read
Route::get('/cast', 'CastController@index'); //ambil data dari database cast 
Route::get('/cast/{cast_id}', 'CastController@show'); //menampilkan data yang telah diambil berdasarkan dari parameter yaitu cast_id

//3. update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //menampilkan form untuk edit pemain film dengan id tertentu
Route::put('/cast/{cast_id}', 'CastController@update'); // 	menyimpan perubahan data pemain film (update) untuk id tertentu

//4. delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); //menghapus data pemain film dengan id tertentu
